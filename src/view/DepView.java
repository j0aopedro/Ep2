package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import edu.unb.fga.dadosabertos.Deputado;

public class DepView extends JFrame {
	
	private JTabbedPane abas = null;
	private JButton searchdep = null;
	private JButton searchid = null;
	private JPanel jp = null;
	private JPanel jp2 = null;
	private JTable tabela1=null;
	private JScrollPane sp = null;
	private DefaultTableModel table = null;
	final String[] COLUNAS = {"Nome", "IdeCadastro", "Partido"};
	
	
	//CONSTRUTOR
	public DepView(final List<Deputado> deputados){
		
		jp2         = new JPanel();
		searchdep   = new JButton("Buscar deputado");
		searchid    = new JButton("Buscar ID");
		abas        = new JTabbedPane();
		table       = new DefaultTableModel();
		tabela1     = new JTable(table);
		jp          = new JPanel();
		sp 			= new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		table.setColumnIdentifiers(COLUNAS);
				
		for(Deputado c : deputados){
			Object[] args = {c.getNome(),c.getIdeCadastro(), c.getPartido()};
			table.addRow(args);
		}
		
		jp         .setBackground(Color.white);
		jp	       .setBounds(0, 0, 800, 500);
		sp         .setBounds(20, 100, 600, 300);
		jp2        .setBackground(Color.white);
		jp2	       .setBounds(0, 0, 800, 500);
		searchdep  .setBounds(10, 50, 40, 50);
		searchid   .setBounds(650, 100, 40, 50);
		
		this    .setSize(800, 500);
		this    .setLocationRelativeTo(null);
		this    .setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		abas    .add("Geral", jp);
		abas    .add("Partidos", jp2);
		this    .add(abas);
		jp      .add(new JScrollPane(tabela1), BorderLayout.CENTER);
		jp      .add(searchdep);
		jp      .add(searchid);
		
		searchdep.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String NOME = JOptionPane.showInputDialog("Digite o nome do Deputado: ");
				System.out.println(NOME);
				/*for(Deputado c : deputados){
					if(c.getNome() == NOME){
						System.out.println("entrou no if");
						final JFrame df = new JFrame();
						final JPanel dp = new JPanel();
						final JLabel dln = new JLabel(); 
						
						df.setSize(300, 500);
						dp.setBackground(Color.white);
						dp.setBounds(0, 0, 300, 500);
						
						dln.setText(c.getNome());
						
						
						df.add(dp);
						df.setVisible(true);
						
					}
				}*/
			}
		});
		
		this    .setVisible(true);
	}
}
