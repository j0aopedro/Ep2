package main;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;

import view.DepView;

public class CamaraDeputados {

	public static void main(String[] args) {

		Camara camara = new Camara();
		try {
			camara.obterDados();
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<Deputado> deputados = camara.getDeputados();
		Deputado deputado = deputados.get(0);
		
		//System.out.println(deputado.getNome());
		//System.out.println(deputado.getIdeCadastro());
		DepView dp = new DepView(deputados);
	}

}
